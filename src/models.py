from torch import nn
import torch


class MLP1(nn.Module):
    def __init__(self):
        super().__init__()

        def block(features_in, features_out, normalise=False, dropout=False):

            layers = [nn.Linear(features_in, features_out)]

            # Different order of BN, Dropout, and non-linearity should be explored!
            # From the literature it seems like there is no canonical way of doing it.

            if normalise:
                layers.append(nn.BatchNorm1d(features_out))

            if dropout:
                layers.append(nn.Dropout(0.2))

            layers.append(nn.LeakyReLU(0.2, inplace=True))

            return layers

        self.model = nn.Sequential(
            *block(626, 2**10, normalise=False, dropout=False),
            *block(2**10, 2**10),
            nn.Linear(2**10, 625)
        )

    def forward(self, stop_grid, delta_t):
        network_input = torch.cat(tensors=(stop_grid, delta_t.view(1)), dim=0)
        return self.model(network_input)
