# Conway's Reverse Game of Life

A little experiment based on the Kaggle challenges set in [2013](https://www.kaggle.com/competitions/conway-s-reverse-game-of-life/) and [2020](https://www.kaggle.com/competitions/conway-s-reverse-game-of-life-2020).

## Ideas

- here
- and here
- ...


## License
GNU Public License 2.0

## Project status
active
